<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('jenisKelamin_id')->unsigned()->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->integer('agama_id')->unsigned()->nullable();
            $table->integer('kewarganegaraan_id')->unsigned()->nullable();
            $table->string('alamat')->nullable();
            $table->string('email')->unique()->nullable();
            $table->integer('fakultas_id')->unsigned();
            $table->integer('prodi_id')->unsigned();
            $table->foreign('jenisKelamin_id')->references('id')->on('jenis_kelamin');
            $table->foreign('agama_id')->references('id')->on('agama');
            $table->foreign('kewarganegaraan_id')->references('id')->on('kewarganegaraan');
            $table->foreign('prodi_id')->references('id')->on('prodi');
            $table->foreign('fakultas_id')->references('id')->on('fakultas');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
