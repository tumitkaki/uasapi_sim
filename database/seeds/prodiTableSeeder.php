<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class prodiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prodi')->insert([
            [
                'nama_prodi' => 'Teknik Informatika',
                'kode_prodi' => '015',
            ],
            [
                'nama_prodi' => 'Ilmu Komputer',
                'kode_prodi' => '025',
            ],
        ]);
    }
}
