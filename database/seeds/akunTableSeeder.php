<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class akunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('akun')->insert([
            [
                'nim' => '1715015013',
                'password' => Hash::make('sandi123'),
                'user_id' => '1',
            ],
            [
                'nim' => '1715015005',
                'password' => Hash::make('hanie123'),
                'user_id' => '2',
            ],
            [
                'nim' => '1715015020',
                'password' => Hash::make('luthfi123'),
                'user_id' => '3',
            ],
            [
                'nim' => '1715015044',
                'password' => Hash::make('abduh123'),
                'user_id' => '4',
            ],
        ]);
    }
}
