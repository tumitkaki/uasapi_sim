<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(jenisKelaminTableSeeder::class);
        $this->call(agamaTableSeeder::class);
        $this->call(fakultas::class);
        $this->call(kewarganegaraanTableSeeder::class);
        $this->call(prodiTableSeeder::class);
        $this->call(userTableSeeder::class);
        $this->call(akunTableSeeder::class);
    }
}
