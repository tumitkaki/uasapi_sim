<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class agamaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agama')->insert([
            [
                'nama_agama' => 'Islam',
            ],
            [
                'nama_agama' => 'Kristen',
            ],
            [
                'nama_agama' => 'Katholik',
            ],
            [
                'nama_agama' => 'Buddha',
            ],
            [
                'nama_agama' => 'Hindu',
            ]
        ]);
    }
}
