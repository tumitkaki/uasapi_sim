<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class kewarganegaraanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kewarganegaraan')->insert([
            [
                'nama_kwg' => 'Warga Negara Indonesia',
            ],
            [
                'nama_kwg' => 'Warga Negara Asing',
            ],
        ]);
    }
}
