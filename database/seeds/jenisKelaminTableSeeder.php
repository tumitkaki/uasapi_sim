<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class jenisKelaminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_kelamin')->insert([
            [
                'nama_jenisKelamin' => 'Laki-Laki',
            ],
            [
                'nama_jenisKelamin' => 'Perempuan',
            ],
        ]);
    }
}
