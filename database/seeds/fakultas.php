<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class fakultas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fakultas')->insert([
            [
                'nama_fakultas' => 'Ilmu Komputer dan Teknologi Informasi',
                'kode_fakultas' => '15',
            ]
        ]);
    }
}
