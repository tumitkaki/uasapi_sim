<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            [
                'nama' => 'Syadan Asandy Nugraha',
                'jenisKelamin_id' => '1',
                'tempat_lahir' => 'Babulu Darat',
                'tanggal_lahir' => '1999-10-18',
                'agama_id' => '1',
                'kewarganegaraan_id' => '1',
                'alamat' => 'Jl Ahim 5',
                'email' => 'syadan.asandy@gmail.com',
                'fakultas_id' => '1',
                'prodi_id' => '1',
            ],
            [
                'nama' => 'Hanie Sri Romandani',
                'jenisKelamin_id' => '2',
                'tempat_lahir' => 'Samarinda',
                'tanggal_lahir' => '1999-01-03',
                'agama_id' => '1',
                'kewarganegaraan_id' => '1',
                'alamat' => 'Gg Kasah 4',
                'email' => 'haniesr9@gmail.com',
                'fakultas_id' => '1',
                'prodi_id' => '1',
            ],
            [
                'nama' => 'M. Luthfi Fahrozi',
                'jenisKelamin_id' => '2',
                'tempat_lahir' => 'Sanga-sanga',
                'tanggal_lahir' => '1995-03-23',
                'agama_id' => '1',
                'kewarganegaraan_id' => '1',
                'alamat' => 'Juanda 7',
                'email' => 'luthfi@gmail.com',
                'fakultas_id' => '1',
                'prodi_id' => '1',
            ],
            [
                'nama' => 'Muhammad Abduh',
                'jenisKelamin_id' => '2',
                'tempat_lahir' => 'Berau',
                'tanggal_lahir' => '1993-11-11',
                'agama_id' => '1',
                'kewarganegaraan_id' => '1',
                'alamat' => 'Suwandi',
                'email' => 'abduh@gmail.com',
                'fakultas_id' => '1',
                'prodi_id' => '1',
            ],
        ]);
    }
}
