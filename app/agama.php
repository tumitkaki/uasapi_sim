<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agama extends Model
{
    protected $table = 'agama';

    public $timestamps = false;

    protected $fillable = [
        'nama_agama'
    ];

    public function user() {
        return $this->hasMany('App\User', 'agama_id', 'id');
    }
}
