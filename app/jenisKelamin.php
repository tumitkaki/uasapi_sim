<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jenisKelamin extends Model
{
    protected $table = 'jenis_kelamin';

    public $timestamps = false;

    protected $fillable = [
        'nama_jenisKelamin',
    ];

    public function user() {
        return $this->hasMany('App\User', 'jenisKelamin_id', 'id');
    }
}
