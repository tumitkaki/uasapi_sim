<?php

namespace App\Http\Controllers;

use App\akun;
use App\prodi;
use App\User;
use App\fakultas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AkunController extends Controller
{

    public function searching(Request $request) {
        $user = DB::table('akun')->join('user', 'akun.user_id', '=', 'user.id')->join('jenis_kelamin', 'user.jenisKelamin_id', '=', 'jenis_kelamin.id')->join('agama', 'user.agama_id', '=', 'agama.id')->join('kewarganegaraan', 'user.kewarganegaraan_id', '=', 'kewarganegaraan.id')->join('fakultas', 'user.fakultas_id', '=', 'fakultas.id')->join('prodi', 'user.prodi_id', '=', 'prodi.id')->select('nim', 'user.nama', 'nama_jenisKelamin as jenis_kelamin', 'nama_agama as agama', 'nama_kwg as kwg', 'nama_fakultas as fakultas', 'nama_prodi as prodi', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'email')->where('nim', $request->nim)->get();
        return $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $prodi = prodi::where('kode_prodi', $request->kode_prodi)->get();
        // $fakultas = fakultas::where('kode_fakultas', $request->kode_fakultas)->get();

        $newUser = new user;
        $newUser->nama = $request->nama;
        $newUser->fakultas_id = fakultas::where('kode_fakultas', $request->kode_fakultas)->get()[0]->id;
        $newUser->prodi_id = prodi::where('kode_prodi', $request->kode_prodi)->get()[0]->id;
        $newUser->save();

        $newAkun = new akun;
        $nim = substr(getdate()["year"], 2).$request->kode_fakultas."0".$request->kode_prodi."00".akun::count()+1;  //membuat nim kode angkatan
        $newAkun->nim = $nim;
        $newAkun->password = Hash::make($nim);
        $newAkun->user_id = user::where('nama', $request->nama)->get()[0]->id;
        $newAkun->save();

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function show(akun $akun)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function edit(akun $akun)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, akun $akun)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function destroy(akun $akun)
    {
        //
    }
}
