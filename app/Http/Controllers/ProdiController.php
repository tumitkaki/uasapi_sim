<?php

namespace App\Http\Controllers;

use App\prodi;
use Illuminate\Http\Request;

class ProdiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Prodi.index')->with('prodi', prodi::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Prodi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newProdi = new prodi;
        $newProdi->nama_prodi = $request->nama_prodi;
        $newProdi->kode_prodi = $request->kode_prodi;
        $newProdi->save();

        return redirect()->route('prodi.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\prodi  $prodi
     * @return \Illuminate\Http\Response
     */
    public function show(prodi $prodi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\prodi  $prodi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Prodi.edit')->with('prodi', prodi::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\prodi  $prodi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newProdi = prodi::find($id);
        $newProdi->nama_prodi = $request->nama_prodi;
        $newProdi->kode_prodi = $request->kode_prodi;
        $newProdi->save();

        return redirect()->route('prodi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\prodi  $prodi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newProdi = prodi::find($id);
        $newProdi->delete();

        return redirect()->route('prodi.index');
    }
}
