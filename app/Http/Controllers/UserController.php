<?php

namespace App\Http\Controllers;

use App\agama;
use App\akun;
use App\jenisKelamin;
use App\kewarganegaraan;
use App\prodi;
use App\User as user;
use App\fakultas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        // $users = user::all();
        $users = DB::table('akun')->leftJoin('user', 'akun.user_id', '=', 'user.id')->leftJoin('jenis_kelamin', 'user.jenisKelamin_id', '=', 'jenis_kelamin.id')->leftJoin('agama', 'user.agama_id', '=', 'agama.id')->leftJoin('kewarganegaraan', 'user.kewarganegaraan_id', '=', 'kewarganegaraan.id')->leftJoin('fakultas', 'user.fakultas_id', '=', 'fakultas.id')->leftJoin('prodi', 'user.prodi_id', '=', 'prodi.id')->select('nim', 'user.nama', 'nama_jenisKelamin as jenis_kelamin', 'nama_agama as agama', 'nama_kwg as kwg', 'nama_fakultas as fakultas', 'nama_prodi as prodi', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'email', 'user.id')->get();
        // return $users;
        // return user::all();
        // return $users;
        return view('User.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fakultas = fakultas::all();
        $prodi = prodi::all();
        return view('User.create', ['fakultas' => $fakultas, 'prodi' => $prodi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $updateUser = new user;
        $updateUser->nama = $request->nama;
        $updateUser->jenisKelamin_id = jenisKelamin::where('nama_jenisKelamin', $request->jenisKelamin)->get()[0]->id;
        $updateUser->tempat_lahir = $request->tempat_lahir;
        $updateUser->tanggal_lahir = $request->tanggal_lahir;
        $updateUser->agama_id = agama::where('nama_agama', $request->agama)->get()[0]->id;
        $updateUser->kewarganegaraan_id = kewarganegaraan::where('nama_kwg', $request->kwg)->get()[0]->id;
        $updateUser->alamat = $request->alamat;
        $updateUser->email = $request->email;
        $updateUser->fakultas_id = fakultas::where('nama_fakultas', $request->fakultas)->get()[0]->id;
        $updateUser->prodi_id = prodi::where('nama_prodi', $request->prodi)->get()[0]->id;
        $updateUser->save();

        return "Data berhasil Masuk";
        // return redirect()
        // return var_dump(DB::table('jenis_kelamin')->where('nama_jenisKelamin', '=', $request->jenisKelamin)->first());


        // return $request->jenisKelamin;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('akun')->leftJoin('user', 'akun.user_id', '=', 'user.id')->leftJoin('jenis_kelamin', 'user.jenisKelamin_id', '=', 'jenis_kelamin.id')->leftJoin('agama', 'user.agama_id', '=', 'agama.id')->leftJoin('kewarganegaraan', 'user.kewarganegaraan_id', '=', 'kewarganegaraan.id')->leftJoin('fakultas', 'user.fakultas_id', '=', 'fakultas.id')->leftJoin('prodi', 'user.prodi_id', '=', 'prodi.id')->select('nim', 'user.nama', 'nama_jenisKelamin as jenis_kelamin', 'nama_agama as agama', 'nama_kwg as kwg', 'nama_fakultas as fakultas', 'nama_prodi as prodi', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'email', 'user.id')->where('user.id', '=', $id)->get();
        return view('mahasiswa.edit', ['user' => $user, 'jenis_kelamin' => jenisKelamin::all(), 'agama' => agama::all(), 'kewarganegaraan' => kewarganegaraan::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateUser = user::find($id);
        $updateUser->nama = $request->nama;
        $updateUser->jenisKelamin_id = $request->jenisKelamin;
        $updateUser->tempat_lahir = $request->tempat_lahir;
        $updateUser->tanggal_lahir = $request->tanggal_lahir;
        $updateUser->agama_id = $request->agama;
        $updateUser->kewarganegaraan_id = $request->kwg;
        $updateUser->alamat = $request->alamat;
        $updateUser->email = $request->email;
        $updateUser->save();

        return redirect()->route('mhs.session', $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = user::find($id);
        $user->delete();

        return redirect()->route('user.index');
    }
}
