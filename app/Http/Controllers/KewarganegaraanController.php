<?php

namespace App\Http\Controllers;

use App\kewarganegaraan;
use Illuminate\Http\Request;

class KewarganegaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Kewarganegaraan.index')->with('kwg', kewarganegaraan::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Kewarganegaraan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newKW = new kewarganegaraan;
        $newKW->nama_kwg = $request->nama_kwg;
        $newKW->save();

        return redirect()->route('kewarganegaraan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function show(kewarganegaraan $kewarganegaraan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Kewarganegaraan.edit')->with('kwg', kewarganegaraan::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newKW = kewarganegaraan::find($id);
        $newKW->nama_kwg = $request->nama_kwg;
        $newKW->save();

        return redirect()->route('kewarganegaraan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newKW = kewarganegaraan::find($id);
        $newKW->delete();

        return redirect()->route('kewarganegaraan.index');
    }
}
