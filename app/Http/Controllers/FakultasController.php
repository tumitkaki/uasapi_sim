<?php

namespace App\Http\Controllers;

use App\fakultas;
use Illuminate\Http\Request;

class FakultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Fakultas.index')->with('fakultas', fakultas::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fakultas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newFakultas = new fakultas;
        $newFakultas->nama_fakultas = $request->nama_fakultas;
        $newFakultas->kode_fakultas = $request->kode_fakultas;
        $newFakultas->save();

        return redirect()->route('fakultas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\fakultas  $fakultas
     * @return \Illuminate\Http\Response
     */
    public function show(fakultas $fakultas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\fakultas  $fakultas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Fakultas.edit')->with('fakultas', fakultas::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\fakultas  $fakultas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newFakultas = fakultas::find($id);
        $newFakultas->nama_fakultas = $request->nama_fakultas;
        $newFakultas->kode_fakultas = $request->kode_fakultas;
        $newFakultas->save();

        return redirect()->route('fakultas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\fakultas  $fakultas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newFakultas = fakultas::find($id);
        $newFakultas->delete();

        return redirect()->route('fakultas.index');
    }
}
