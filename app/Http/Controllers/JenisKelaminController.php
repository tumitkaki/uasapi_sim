<?php

namespace App\Http\Controllers;

use App\jenisKelamin;
use Illuminate\Http\Request;

class JenisKelaminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('JenisKelamin.index')->with('jeniskelamin', jenisKelamin::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('JenisKelamin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newJK = new jenisKelamin;
        $newJK->nama_jenisKelamin = $request->nama_jenisKelamin;
        $newJK->save();

        return redirect()->route('jeniskelamin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jenisKelamin  $jenisKelamin
     * @return \Illuminate\Http\Response
     */
    public function show(jenisKelamin $jenisKelamin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jenisKelamin  $jenisKelamin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('JenisKelamin.edit')->with('jeniskelamin', jenisKelamin::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jenisKelamin  $jenisKelamin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newJK = jenisKelamin::find($id);
        $newJK->nama_jenisKelamin = $request->nama_jenisKelamin;
        $newJK->save();

        return redirect()->route('jeniskelamin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jenisKelamin  $jenisKelamin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newJK = jenisKelamin::find($id);
        $newJK->delete();

        return redirect()->route('jeniskelamin.index');
    }
}
