<?php

namespace App\Http\Controllers;

use App\agama;
use Illuminate\Http\Request;

class AgamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return agama::all();
        // return response()->json(agama::all());
        return view("Agama.index", ["agama" => agama::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('Agama.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newAgama = new agama;
        $newAgama->nama_agama = $request->nama_agama;
        $newAgama->save();

        return redirect()->route('agama.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\agama  $agama
     * @return \Illuminate\Http\Response
     */
    public function show(agama $agama)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\agama  $agama
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('agama.edit', ['agama' => agama::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\agama  $agama
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateAgama = agama::find($id);
        $updateAgama->nama_agama = $request->nama_agama;
        $updateAgama->save();

        return redirect()->route('agama.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\agama  $agama
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agama = agama::find($id);
        $agama->delete();

        return redirect()->route('agama.index');
    }
}
