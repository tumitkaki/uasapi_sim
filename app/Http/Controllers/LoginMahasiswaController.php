<?php

namespace App\Http\Controllers;

use App\akun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginMahasiswaController extends Controller
{
    public function index()
    {
        return view('loginMhs');
    }

    public function login(Request $request)
    {
        if ($this->validasi($request)) {
            // return $this->getUser($request);
            return view('mahasiswa.index')->with('user', $this->getUser($request));
        } else {
            return redirect()->route('login.index');
        }
    }

    public function session(Request $request)
    {
        // return $request;
        if ($request->session()->has('nama')) {
            return view('mahasiswa.index')->with('user', $this->getUser($request));
        }
        else {
            return redirect()->route('login.index');
        }
    }

    public function validasi(Request $request)
    {
        $akun = akun::where('nim', '=', $request->nim)->first();

        if (empty($akun)){}
        else {
            if (Hash::check($request->password, $akun->password)) {
                return True;
            }
        }
        return False;
    }

    public function home($id)
    {
        return $id;
        // return view('mahasiswa.index')->with('user', DB::table('akun')->leftJoin('user', 'akun.user_id', '=', 'user.id')->leftJoin('jenis_kelamin', 'user.jenisKelamin_id', '=', 'jenis_kelamin.id')->leftJoin('agama', 'user.agama_id', '=', 'agama.id')->leftJoin('kewarganegaraan', 'user.kewarganegaraan_id', '=', 'kewarganegaraan.id')->leftJoin('fakultas', 'user.fakultas_id', '=', 'fakultas.id')->leftJoin('prodi', 'user.prodi_id', '=', 'prodi.id')->select('nim', 'user.nama', 'nama_jenisKelamin as jenis_kelamin', 'nama_agama as agama', 'nama_kwg as kwg', 'nama_fakultas as fakultas', 'nama_prodi as prodi', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'email', 'user.id')->where('id', '=', $id)->get());
    }

    public function getUser(Request $request)
    {
        $akun = DB::table('akun')->leftJoin('user', 'akun.user_id', '=', 'user.id')->leftJoin('jenis_kelamin', 'user.jenisKelamin_id', '=', 'jenis_kelamin.id')->leftJoin('agama', 'user.agama_id', '=', 'agama.id')->leftJoin('kewarganegaraan', 'user.kewarganegaraan_id', '=', 'kewarganegaraan.id')->leftJoin('fakultas', 'user.fakultas_id', '=', 'fakultas.id')->leftJoin('prodi', 'user.prodi_id', '=', 'prodi.id')->select('nim', 'user.nama', 'nama_jenisKelamin as jenis_kelamin', 'nama_agama as agama', 'nama_kwg as kwg', 'nama_fakultas as fakultas', 'nama_prodi as prodi', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'email', 'user.id')->where('nim', '=', $request->nim)->get();
        // $akun = $this->buatSession($request);
        return $akun;
    }

    public function buatSession(Request $request) {
        $request->session()->put('nama', $request->nama);
        return $request;
    }

    public function hapusSession(Request $request) {
        $request->session()->forget('nama');
        return $request;
	}
}
