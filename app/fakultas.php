<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fakultas extends Model
{
    protected $table = 'fakultas';

    public $timestamps = false;

    protected $fillable = [
        'nama_fakultas', 'kode_fakultas'
    ];

    public function user() {
        return $this->hasMany('App\User', 'fakultas_id', 'id');
    }
}
