<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kewarganegaraan extends Model
{
    protected $table = 'kewarganegaraan';
    public $timestamps = false;
    protected $fillable = [
        'nama_kwg',
    ];

    public function user() {
        return $this->hasMany('App\User', 'kewarganegaraan_id', 'id');
    }
}
