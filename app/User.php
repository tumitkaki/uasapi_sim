<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "user";

    protected $fillable = [
        'nama', 'jenisKelamin_id', 'tempat_lahir', 'tanggal_lahir', 'agama_id', 'kewarganegaraan_id', 'alamat', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function akun() {
        return $this->belongsTo('App\akun', 'user_id', 'id');
    }

    public function agama() {
        return $this->belongsTo('App\agama', 'agama_id', 'id');
    }

    public function fakultas() {
        return $this->belongsTo('App\fakultas', 'fakultas_id', 'id');
    }

    public function prodi() {
        return $this->belongsTo('App\prodi', 'prodi_id', 'id');
    }

    public function jenis_kelamin() {
        return $this->belongsTo('App\jenisKelamin', 'jenisKelamin_id', 'id');
    }

    public function kewarganegaraan() {
        return $this->belongsTo('App\kewarganegaraan', 'kewarganegaraan_id', 'id');
    }
}
