<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prodi extends Model
{
    protected $table = 'prodi';
    public $timestamps = false;
    protected $fillable = [
        'nama_prodi', 'kode_prodi'
    ];

    public function user() {
        return $this->hasMany('App\User', 'prodi_id', 'id');
    }
}
