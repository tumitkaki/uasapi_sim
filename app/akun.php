<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class akun extends Model
{
    protected $table = 'akun';

    public $timestamps = false;

    protected $fillable = [
        'nim', 'password', 'user_id'
    ];

    public function user() {
        return $this->hasOne('App\User', 'user_id', 'id');
    }
}
