<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit | Mahasiswa</title>
</head>
<body>
    <h1>EDIT MAHASISWA</h1>
    @foreach ($user as $user)
        <form action="{{ route('user.update', $user->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <h2>{{ $user->nim }}</h2>
            <div>
                <label for="nama">Nama</label>
                <input type="text" name="nama" required type="text" placeholder="Nama" autocomplete="off" value="{{ $user->nama }}">
            </div>
            <div>
                <label for="jenis_kelamin">Jenis Kelamin</label>
                <select name="jenis_kelamin" id="jenis_kelamin">
                    @foreach ($jenis_kelamin as $jk)
                    <option value="{{ $jk->id }}">{{ $jk->nama_jenisKelamin }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" value="{{ $user->tempat_lahir }}">
            </div>
            <div>
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" value="{{ $user->tanggal_lahir }}">
            </div>
            <div>
                <label for="agama">Agama</label>
                <select name="agama" id="agama">
                    @foreach ($agama as $agama)
                        <option value="{{ $agama->id }}">{{ $agama->nama_agama }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="alamat">Alamat</label>
                <input type="text" name="alamat" id="alamat" value="{{ $user->alamat }}">
            </div>
            <div>
                <label for="kwg">Kewarganegaraan</label>
                <select name="kwg" id="kwg">
                    @foreach ($kewarganegaraan as $kwg)
                        <option value="{{ $kwg->id }}">{{ $kwg->nama_kwg }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="{{ $user->email }}">
            </div>
            <div>
                <input type="submit" value="Simpan">
            </div>
        </form>
    @endforeach
</body>
</html>
