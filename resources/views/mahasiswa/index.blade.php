<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index | Mahasiswa</title>
</head>
<body>
    <table border="1">
            <th>NIM</th>
            <th>NAMA</th>
            <th>JENIS KELAMIN</th>
            <th>TEMPAT LAHIR</th>
            <th>TANGGAL LAHIR</th>
            <th>AGAMA</th>
            <th>KEWARGANEGARAAN</th>
            <th>ALAMAT</th>
            <th>EMAIL</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
            <th>AKSI</th>
        @foreach ($user as $user)
            <tr>
                <td>{{ $user->nim }}</td>
                <td>{{ $user->nama }}</td>
                <td>{{ $user->jenis_kelamin }}</td>
                <td>{{ $user->tempat_lahir }}</td>
                <td>{{ $user->tanggal_lahir }}</td>
                <td>{{ $user->agama }}</td>
                <td>{{ $user->kwg }}</td>
                <td>{{ $user->alamat }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->fakultas }}</td>
                <td>{{ $user->prodi }}</td>
                <td>
                    <a href="{{ route('user.edit', $user->id) }}"><button type="submit" name="button">Edit</button> </a>
                </td>

            </tr>
            @endforeach
        </table>
</body>
</html>
