<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('title')
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('js/datatables.min.css') }}"/> --}}
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/DataTables-1.10.20/js/jquery.dataTables.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/DataTables-1.10.20/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/DataTables-1.10.20/css/dataTables.bootstrap.css') }}">
</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Admin</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('user.index') }}">Mahasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('fakultas.index') }}">Fakultas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('prodi.index') }}">Program Studi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('agama.index') }}">Agama</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('jeniskelamin.index') }}">Jenis Kelamin</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('kewarganegaraan.index') }}">Kewarganegaraan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Logout</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
    @yield('konten')
    <script type="text/javascript" charset="utf8">
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
</body>
</html>
