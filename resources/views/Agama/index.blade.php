@php
    $no = 1;
@endphp

@extends('layouts.index')

@section('title')
    <title>Index | Agama</title>
@endsection

@section('konten')
    <div style="width: 90%; margin: auto;">
        <div class="mb-3 mt-4 d-flex justify-content-center">
            <div class="btn-dark col-md-3 justify-content-center d-flex rounded">
                <h1>List Agama</h1>
            </div>
        </div>
        <a href="{{ route('agama.create') }}"><button class="btn btn-dark mb-3" type="submit">BUAT</button></a>
        <table id="table_id" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th>AKSI</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($agama as $agama)
                <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $agama->nama_agama }}</td>
                    <td>
                        <a href="{{ route('agama.edit', $agama['id']) }}"><button type="submit" name="button">Edit</button> </a>
                        <form class="" action="{{ route('agama.destroy', $agama['id']) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" name="button">Hapus</button>
                        </form>
                    </td>
                    @php
                        $no++
                    @endphp
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
