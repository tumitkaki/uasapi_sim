@extends('layouts.index')

@section('title')
    <title>Edit | Agama</title>
@endsection

@section('konten')

    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-4 justify-content-center d-flex rounded">
                <h1>Edit AGAMA</h1>
            </div>
        </div>
        <form action="{{ route('agama.update', $agama->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="nama_agama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama_agama" required type="text" placeholder="Masukkan Nama" autocomplete="off" value="{{ $agama->nama_agama }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
