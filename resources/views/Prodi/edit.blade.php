@extends('layouts.index')

@section('title')
    <title>Create | Program Studi</title>
@endsection

@section('konten')
    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-5 justify-content-center d-flex rounded">
                <h1>Tambah Program Studi</h1>
            </div>
        </div>
        <form action="{{ route('prodi.update', $prodi->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="nama_prodi">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama_prodi" required value="{{ $prodi->nama_prodi }}">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="kode_prodi">Kode</label>
                    <input type="text" class="form-control" id="kode" placeholder="Masukkan Kode" name="kode_prodi" required value="{{ $prodi->kode_prodi }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
