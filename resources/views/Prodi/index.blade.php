@php
    $no = 1;
@endphp

@extends('layouts.index')

@section('title')
    <title>Index | Program Studi</title>
@endsection

@section('konten')
    <div style="width: 90%; margin: auto;">
        <div class="mb-3 mt-4 d-flex justify-content-center">
            <div class="btn-dark col-md-4 justify-content-center d-flex rounded">
                <h1>List Program Studi</h1>
            </div>
        </div>
    <a href="{{ route('prodi.create') }}"><button class="btn btn-dark mb-3" type="submit">BUAT</button></a>
    <table id="table_id" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>NO</th>
                <th>NAMA</th>
                <th>KODE</th>
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($prodi as $prodi)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $prodi->nama_prodi }}</td>
                <td>{{ $prodi->kode_prodi }}</td>
                <td>
                    <a href="{{ route('prodi.edit', $prodi['id']) }}"><button type="submit" name="button">Edit</button> </a>
                    <form class="" action="{{ route('prodi.destroy', $prodi['id']) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" name="button">Hapus</button>
                    </form>
                </td>
                @php
                    $no++
                @endphp
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
