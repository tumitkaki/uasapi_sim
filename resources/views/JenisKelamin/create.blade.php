@extends('layouts.index')

@section('title')
    <title>Create | Jenis Kelamin</title>
@endsection

@section('konten')
    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-5 justify-content-center d-flex rounded">
                <h1>Tambah Jenis Kelamin</h1>
            </div>
        </div>
        <form action="{{ route('jeniskelamin.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="nama_fakultas">Jenis Kelamin</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Jenis Kelamin" name="nama_jenisKelamin" required>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
