@extends('layouts.index')

@section('title')
    <title>Edit | Jenis Kelamin</title>
@endsection

@section('konten')
    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-5 justify-content-center d-flex rounded">
                <h1>Edit Jenis Kelamin</h1>
            </div>
        </div>
        <form action="{{ route('jeniskelamin.update', $jeniskelamin->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="nama_fakultas">Jenis Kelamin</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Jenis Kelamin" name="nama_jenisKelamin" required value="{{ $jeniskelamin->nama_jenisKelamin }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
