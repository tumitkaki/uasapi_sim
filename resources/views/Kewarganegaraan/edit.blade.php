@extends('layouts.index')

@section('title')
    <title>Edit | Kewarganegaraan</title>
@endsection

@section('konten')
    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-6 justify-content-center d-flex rounded">
                <h1>Edit Kewarganegaraan</h1>
            </div>
        </div>
        <form action="{{ route('kewarganegaraan.update', $kwg->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="nama_fakultas">Kewarganegaraan</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan kewarganegaraan" name="nama_kwg" required value="{{ $kwg->nama_kwg }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
