@php
    $no = 1;
@endphp

@extends('layouts.index')

@section('title')
    <title>Index | Kewarganegaraan</title>
@endsection

@section('konten')
    <div style="width: 90%; margin: auto;">
        <div class="mb-3 mt-4 d-flex justify-content-center">
            <div class="btn-dark col-md-5 justify-content-center d-flex rounded">
                <h1>List Kewarganegaraan</h1>
            </div>
        </div>
    <a href="{{ route('kewarganegaraan.create') }}"><button class="btn btn-dark mb-3" type="submit">BUAT</button></a>
    <table id="table_id" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>NO</th>
                <th>NAMA</th>
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($kwg as $kwg)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $kwg->nama_kwg }}</td>
                <td>
                    <a href="{{ route('kewarganegaraan.edit', $kwg['id']) }}"><button type="submit" name="button">Edit</button> </a>
                    <form class="" action="{{ route('kewarganegaraan.destroy', $kwg['id']) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" name="button">Hapus</button>
                    </form>
                </td>
                @php
                    $no++
                @endphp
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
