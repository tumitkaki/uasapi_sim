<!DOCTYPE html>
<html class="no-js" lang="zxx"><!-- Mirrored from colorlib.com/preview/theme/eclipse/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Mar 2019 03:41:02 GMT --><!-- Added by HTTrack --><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><!-- /Added by HTTrack -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="http://elearning.fkti.unmul.ac.id/img/fav.png">
	<meta name="author" content="colorlib">
	<meta charset="UTF-8">
	<title>SIA Mulawarman</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>
<body>
    <nav class="navbar navbar-expand-lg  navbar-light bg-dark" style="padding: 20pt 0;">
        <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                <h2 style="color: white; font-family: 'Times New Roman', Times, serif">SIA Mulawarman</h2>
            </a>
        </div>
    </nav>

	<section>
		<div style="padding-top: 10%;" class="row">
			<div class="col-md-6" align="right">
                <a href="{{ route('login.index') }}">
					<img style="max-width: 30%; margin-right: 5%;" src="{{ asset('images/student.png') }}">
					<h1 style="margin-right: 3%; font-weight: bold; font-family:verdana; ">Mahasiswa</h1>
				</a>
			</div>
			<div class="col-md-6" align="left">
                <a href="{{ url('home') }}">
					<img style="max-width: 30%; margin-left: 5%;" src="{{ asset('images/professor.png') }}">
                    <h1 style="margin-left: 9%; font-weight: bold; font-family:verdana; ">Dosen</h1>
				</a>
			</div>
		</div>
	</section>
</html>
