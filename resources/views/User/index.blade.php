@extends('layouts.index')
@php
    $no = 1;
@endphp
@section('title')
    <title>Index | Mahasiswa</title>
@endsection
@section('konten')
    <div class="mb-3 mt-4 d-flex justify-content-center">
        <div class="btn-dark col-md-4 justify-content-center d-flex rounded">
            <h1>List Mahasiswa</h1>
        </div>
    </div>
    <a href="{{ route('user.create') }}"><button class="btn btn-dark mb-3" type="submit">BUAT</button></a>
    <table id="table_id" class="table table-striped table-bordered" style="font-size: 10pt;">
        <thead>
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>JENIS KELAMIN</th>
                <th>TEMPAT LAHIR</th>
                <th>TANGGAL LAHIR</th>
                <th>AGAMA</th>
                <th>KEWARGANEGARAAN</th>
                <th>ALAMAT</th>
                <th>EMAIL</th>
                <th>FAKULTAS</th>
                <th>PROGRAM STUDI</th>
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $user->nim }}</td>
                <td>{{ $user->nama }}</td>
                <td>{{ $user->jenis_kelamin }}</td>
                <td>{{ $user->tempat_lahir }}</td>
                <td>{{ $user->tanggal_lahir }}</td>
                <td>{{ $user->agama }}</td>
                <td>{{ $user->kwg }}</td>
                <td>{{ $user->alamat }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->fakultas }}</td>
                <td>{{ $user->prodi }}</td>
                <td>
                    {{-- <a href="{{ route('user.edit', $user->id) }}"><button type="submit" name="button">Edit</button> </a> --}}
                    <form class="" action="{{ route('user.destroy', $user->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" name="button">Hapus</button>
                    </form>
                </td>

                @php
                    $no++
                @endphp
            </tr>
            @endforeach
        </tbody>
        </table>
@endsection
