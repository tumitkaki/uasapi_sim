@extends('layouts.index')

@section('title')
    <title>Create | Mahasiswa</title>
@endsection

@section('konten')
    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-5 justify-content-center d-flex rounded">
                <h1>Tambah Mahasiswa</h1>
            </div>
        </div>
        <form action="{{ route('akun.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" required>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="kode_fakultas">Fakultas</label>
                    <select class="form-control" name="kode_fakultas" id="">
                        @foreach ($fakultas as $each)
                            <option value="{{ $each->kode_fakultas }}">{{ $each->nama_fakultas }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="kode_prodi">Program Studi</label>
                    <select class="form-control" name="kode_prodi" id="">
                        @foreach ($prodi as $each)
                            <option value="{{ $each->kode_prodi }}">{{ $each->nama_prodi }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
