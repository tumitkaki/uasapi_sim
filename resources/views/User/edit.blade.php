<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit | Mahasiswa</title>
</head>
<body>
    <h1>EDIT MAHASISWA</h1>
    <form action="{{ route('user.update', $user->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <div>
            <label for="nama_prodi">Nama</label>
            <input type="text" name="nama" required type="text" placeholder="Nama" autocomplete="off" value="{{ $user->nama }}">
        </div>
        {{-- <div>
            <label for="kode_prodi">Kode</label>
            <input type="text" name="kode_prodi" required type="text" placeholder="Nama" autocomplete="off" value="{{ $prodi->kode_prodi }}">
        </div> --}}
        <div>
            <input type="submit" value="Simpan">
        </div>
    </form>
</body>
</html>
