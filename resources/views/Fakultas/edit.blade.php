@extends('layouts.index')

@section('title')
    <title>Edit | FAKULTAS</title>
@endsection

@section('konten')
    <div class="container">
        <div class="mb-3 mt-4 d-flex">
            <div class="btn-dark col-md-4 justify-content-center d-flex rounded">
                <h1>Edit Fakultas</h1>
            </div>
        </div>
        <form action="{{ route('fakultas.update', $fakultas->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="nama_fakultas">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama_fakultas" required value="{{ $fakultas->nama_fakultas }}">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="kode_fakultas">Kode</label>
                    <input type="text" class="form-control" id="kode" placeholder="Masukkan Kode" name="kode_fakultas" required value="{{ $fakultas->kode_fakultas }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
