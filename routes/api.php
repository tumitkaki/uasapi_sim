<?php

use Illuminate\Http\Request;
// use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/user', 'UserController@index');
Route::get('/akun/search/', 'AkunController@searching')->name('akun.search');


// Route::get('/agama', 'AgamaController@index');
Route::get('agama/create', 'AgamaController@create')->name('agama.create');
Route::get('agama/edit/{id}', 'AgamaController@edit')->name('agama.edit');
Route::get('fakultas/create', 'FakultasController@create')->name('fakultas.create');
Route::get('fakultas/edit/{id}', 'FakultasController@edit')->name('fakultas.edit');
Route::get('jeniskelamin/create', 'JenisKelaminController@create')->name('jeniskelamin.create');
Route::get('jeniskelamin/edit/{id}', 'JenisKelaminController@edit')->name('jeniskelamin.edit');
Route::get('prodi/create', 'ProdiController@create')->name('prodi.create');
Route::get('prodi/edit/{id}', 'ProdiController@edit')->name('prodi.edit');
Route::get('kewarganegaraan/create', 'KewarganegaraanController@create')->name('kewarganegaraan.create');
Route::get('kewarganegaraan/edit/{id}', 'KewarganegaraanController@edit')->name('kewarganegaraan.edit');
Route::get('user/create', 'userController@create')->name('user.create');
Route::get('user/edit/{id}', 'userController@edit')->name('user.edit');
Route::get('akun/create', 'AkunController@create')->name('akun.create');
Route::get('akun/edit/{id}', 'AkunController@edit')->name('akun.edit');
// Route::get('agama/{id}', 'AgamaController@update');
// Route::get('agama/{id}', 'AgamaController@delete');
// Route::get('api/agama', 'AgamaController@create')->name('agama.create');
Route::apiResource('akun', 'AkunController');
Route::apiResource('user', 'UserController');
Route::apiResource('agama', 'AgamaController');
Route::apiResource('fakultas', 'FakultasController');
Route::apiResource('prodi', 'ProdiController');
Route::apiResource('jeniskelamin', 'JenisKelaminController');
Route::apiResource('kewarganegaraan', 'KewarganegaraanController');
