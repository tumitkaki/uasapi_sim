<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('index');
});
// Route::get('api/agama', 'AgamaController@create')->name('agama.create');
// Route::resource('agama', 'AgamaController');
// Route::resource('user', 'UserController');

// Auth::routes();
Route::get('/mahasiswa/session', 'LoginMahasiswaController@session')->name('mhs.session');
Route::get('/mahasiswa', 'LoginMahasiswaController@home')->name('mhs.home');
Route::get('/login/mhs', 'LoginMahasiswaController@index')->name('login.index');
Route::post('/login/mhs', 'LoginMahasiswaController@login')->name('login.mhs');

Route::get('/home', function() {
    return view('home');
});
// Route::get('/home', 'HomeController@index')->name('home');
